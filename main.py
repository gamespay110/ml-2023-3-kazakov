import torch

from dataHandler import DataHandler
from featureExtractor import FeatureExtractor
from model import Model
from preprocessor import Preprocessor

handler = DataHandler("Data/labeled.csv")
preprocessor = Preprocessor()

# Загружаем данные
handler.load_data()
# Предобрабатываем каждый комментарий в данных
for i in range(len(handler.data)):
    comment = handler.data.loc[
        i, "comment"
    ]  # предполагается, что столбец с комментариями называется 'comment'
    tokens = preprocessor.preprocess(comment)
    handler.data.loc[i, "comment"] = " ".join(tokens)
handler.data = handler.data.drop(handler.data[handler.data["comment"] == ""].index)
# Разделяем данные на обучающую и тестовую выборки
train_data, test_data = handler.split_data()

# Создаем объекты классов FeatureExtractor и Model
extractor = FeatureExtractor()
model = Model()

train_comments = train_data["comment"].tolist()
test_comments = test_data["comment"].tolist()

# Извлечение признаков из обучающих данных
train_features = extractor.extract_features(train_comments)

# Извлечение признаков из тестовых данных
test_features = extractor.extract_features(test_comments)

# Извлекаем признаки из обучающих данных
train_features = extractor.extract_features(train_data["comment"])

# Конвертируем метки в формат, подходящий для обучения модели
train_labels = torch.tensor(train_data["label"].values)

# Обучаем модель
model.train(train_features, train_labels)

# Извлекаем признаки из тестовых данных
test_features = extractor.extract_features(test_data["comment"])

# Получаем предсказания модели для тестовых данных
predictions = model.predict(test_features)
