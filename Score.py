from nltk.translate import metrics


def get_scores(y_truely, y_predict, y_predict_proba):
    """
    Выводит на печать основные метрики качества модели: precision, recall, f_1, accuracy, ROC_AUC_SCORE
    :param y_truely - настоящие значения таргета:
    :param y_predict - предсказываемые значения таргета:
    :param y_predict_proba - предсказываемые значения вероятности:
    :return:
    """
    pr = metrics.precision_score(y_truely, y_predict)
    r = metrics.recall_score(y_truely, y_predict)
    f1 = metrics.f1_score(y_truely, y_predict)
    ac = metrics.accuracy_score(y_truely, y_predict)
    rc = metrics.roc_auc_score(y_truely, y_predict_proba)
    print(f" precision: {pr}")
    print(f" recall : {r}")
    print(f" f1_score: {f1}")
    print(f" accuracy_score: {ac}")
    print(f" roc_auc_score: {rc}")
    return [pr, r, f1, ac, rc]
