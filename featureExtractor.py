from transformers import BertModel, BertTokenizer


class FeatureExtractor:
    def __init__(self, model_name='DeepPavlov/rubert-base-cased'):
        self.tokenizer = BertTokenizer.from_pretrained(model_name)
        self.model = BertModel.from_pretrained(model_name)


    def extract_features(self, data, max_length=200):
        ''' Извлечение признаков для каждого комментария в данных
            Data - текст после предобработки в формате pd.dataframe
            max_lenght - указывается максимальная длина строки до которой будет обрезан/удленен текст
        '''
        features = []
        for comment in data:  # теперь 'data' - это список комментариев
            inputs = self.tokenizer.encode_plus(comment, add_special_tokens=True, max_length=max_length,
                                                padding='max_length', truncation=True, return_tensors='pt')
            outputs = self.model(**inputs)
            features.append(outputs.last_hidden_state)
        return features
