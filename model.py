from transformers import BertForSequenceClassification, AdamW
import torch
from torch.utils.data import DataLoader, TensorDataset


class Model:
    def __init__(self,
                 model_name="DeepPavlov/rubert-base-cased",
                 num_labels=2):
        self.device = (torch.device(
            "cuda" if torch.cuda.is_available() else "cpu")
        )
        print(f"Model will run on: {self.device}")
        self.model = BertForSequenceClassification.from_pretrained(
            model_name, num_labels=num_labels
        ).to(self.device)
        self.optimizer = AdamW(self.model.parameters(), lr=1e-5)

    def train(self, features, labels, batch_size=4):
        # Создание DataLoader для управления пакетами
        dataset = TensorDataset(features, labels)
        dataloader = DataLoader(dataset, batch_size=batch_size)

        # Обучение модели
        for batch in dataloader:
            batch_features, batch_labels = batch

            # Выполнение прямого прохода модели
            outputs = self.model(
                batch_features.to(self.device),
                labels=batch_labels.to(self.device)
            )

            # Вычисление потерь
            loss = outputs.loss

            # Выполнение обратного прохода и оптимизации
            loss.backward()
            self.optimizer.step()
            self.optimizer.zero_grad()

    def predict(self, features, batch_size=4):
        # Создание DataLoader для управления пакетами
        dataset = TensorDataset(features)
        dataloader = DataLoader(dataset, batch_size=batch_size)

        # Получение предсказаний модели
        predictions = []
        for batch in dataloader:
            batch_features = batch[0]

            # Выполнение прямого прохода модели
            outputs = self.model(batch_features.to(self.device))

            # Добавление предсказаний модели
            batch_predictions = torch.argmax(outputs.logits, dim=-1)
            predictions.extend(batch_predictions.cpu().numpy())

        return predictions
