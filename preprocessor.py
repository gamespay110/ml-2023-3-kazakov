from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import re


class Preprocessor:
    def __init__(self):
        self.stemmer = PorterStemmer()
        self.stopwords = set(stopwords.words("russian"))

    def clean_text(self, text):
        punctuation_marks = [
            "!",
            ",",
            "(",
            ")",
            ":",
            "-",
            "?",
            ".",
            "..",
            "...",
            "@",
            ";",
            "/",
            "''",
            "``",
            "№",
            "«",
            "»",
            "–",
        ]
        tokenized_text = word_tokenize(
            text.lower().replace("ё", "e"), language="russian"
        )
        clean_text = [
            word
            for word in tokenized_text
            if word not in punctuation_marks and word != "\n"
        ]
        r = re.compile("[а-яА-Я]+")
        cleaned_text = " ".join([w for w in filter(r.match, clean_text)])
        return cleaned_text

    def tokenize(self, text):
        # Токенизация
        return word_tokenize(text)

    def remove_stopwords(self, tokens):
        # Удаление стоп-слов
        return [token for token in tokens if token not in self.stopwords]

    def stem(self, tokens):
        # Стемминг
        return [self.stemmer.stem(token) for token in tokens]

    def preprocess(self, text):
        # Полный процесс предобработки
        text = self.clean_text(text)
        tokens = self.tokenize(text)
        tokens = self.remove_stopwords(tokens)
        tokens = self.stem(tokens)
        return tokens
