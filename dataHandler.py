import pandas as pd
from sklearn.model_selection import train_test_split


class DataHandler:
    def __init__(self, filepath):
        self.filepath = filepath
        self.data = None

    def load_data(self):
        # Загрузка данных из CSV-файла
        self.data = pd.read_csv(self.filepath)

    def split_data(self, test_size=0.2, random_state=52):
        # Разделение данных на обучающую и тестовую выборки
        train_data, test_data = train_test_split(self.data, test_size=test_size, random_state=random_state)
        return train_data, test_data

    def save_data(self, data, filepath):
        # Сохранение данных в CSV-файл
        data.to_csv(filepath, index=False)